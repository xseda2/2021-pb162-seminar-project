PB162 Java
=============

At the left upper part you can see the list of branches with the assignments.

You can find all necessary information [here](https://gitlab.fi.muni.cz/pb162/pb162-course-info/wikis/home).

**Private information for teachers is [here](https://gitlab.fi.muni.cz/pb162/pb162-teachers-info/wikis/home)**.

# Current branch `master`

This branch consist of working _Hello world_ project with the passing tests. Try it! 